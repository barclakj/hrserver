package com.pxa.hr.service;


import com.pxa.hr.model.Employee;
import com.pxa.hr.views.EmployeeViewAPIResponse;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
/**
 * HR service interface.
 */
public interface HRService {

    /**
     * Returns an individual employee.
     * @param employeeId
     * @return
     */
    Employee getEmployee(final int employeeId);


    /**
     * Returns an individual employees details.
     * @param employeeId
     * @return
     */
    EmployeeViewAPIResponse getEmployeeDetails(final int employeeId);

    /**
     * Returns all employee details which report to the specified employee.
     * @param employeeId
     * @return
     */
    List<Employee> getEmployeeReports(final int employeeId);

    /**
     * Adds a new employee.
     * @param employee
     */
    void addNewEmployee(final Employee employee) throws EmployeeExistsException;

    /**
     * Deletes an existing employee
     * @param id
     */
    void deleteEmployee(final int id);

    /**
     * GraphQL query..
     * @param query
     * @return
     */
    Map query(final String query);
}
