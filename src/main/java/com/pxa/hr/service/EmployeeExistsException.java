package com.pxa.hr.service;

public class EmployeeExistsException extends Exception {

    public EmployeeExistsException() {
        super();
    }

    public EmployeeExistsException(String message) {
        super(message);
    }

    public EmployeeExistsException(String message, Throwable cause) {
        super(message, cause);
    }

    public EmployeeExistsException(Throwable cause) {
        super(cause);
    }

    protected EmployeeExistsException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
