package com.pxa.hr.service;

import com.pxa.hr.graph.HRGraphProvider;
import com.pxa.hr.model.Employee;
import com.pxa.hr.views.EmployeeViewAPIResponse;
import graphql.ExecutionResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Component
/**
 * Basic implementation of the hr service.
 *
 * This implementation simple stores employee details in memory rather than
 * using a proper datasource.
 */
public class HRServiceImpl implements HRService {
    @Autowired
    HRGraphProvider graphProvider;

    private Map<Integer, Employee> allEmployees = new HashMap<Integer, Employee>();

    @Override
    public Employee getEmployee(final int employeeId) {
        log.info("Fetching data for employee by id: {}", employeeId);
        Employee employee = this.allEmployees.get(employeeId);
        return employee;
    }

    @Override
    public EmployeeViewAPIResponse getEmployeeDetails(final int employeeId) {
        log.info("Fetching data for employee by id: {}", employeeId);
        Employee employee = this.allEmployees.get(employeeId);
        List<Employee> reports = this.getEmployeeReports(employeeId);
        return new EmployeeViewAPIResponse(employee, reports);
    }

    @Override
    public List<Employee> getEmployeeReports(final int employeeId) {
        log.info("Fetching reports data for employee by id: {}", employeeId);
        return allEmployees.values()
                .stream()
                .filter(emp -> emp.getReportsTo()== employeeId)
                .collect(Collectors.toList());
    }


    @Override
    public void addNewEmployee(final Employee employee) throws EmployeeExistsException {
        // todo: should validate employee here
        log.info("Adding employee by id: {}", employee.getId());
        Employee existingEmp = this.getEmployee(employee.getId());
        if (existingEmp!=null) {
            log.warn("Exmployee already exists!");
            throw new EmployeeExistsException("Employee already exists!");
        } else {
            allEmployees.put(employee.getId(), employee);
        }
    }

    @Override
    public void deleteEmployee(final int id) {
        log.info("Deleting employee by id: {}", id);
        allEmployees.remove(id);
    }

    @Override
    public Map query(final String query) {
        log.info("Querying data with query: {}", query);
        ExecutionResult result = graphProvider.getGraphQL().execute(query);
        return result.getData();
    }

}
