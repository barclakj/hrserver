package com.pxa.hr.graph;

import graphql.GraphQL;
import graphql.schema.GraphQLSchema;
import graphql.schema.idl.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.*;

@Component
/**
 * GraphQl provider basde on employee schema.
 */
public class HRGraphProvider {
    public static final String schemaFile = "employee-schema.graphqls";

    private GraphQL graphQL;

    @Autowired
    private HRDataFetchers dataFetchers;

    @Bean
    public GraphQL getGraphQL() {
        return graphQL;
    }

    @PostConstruct
    public void init() throws IOException {
        InputStream is = getClass().getClassLoader().getResourceAsStream(schemaFile);
        assert(is!=null);
        GraphQLSchema schema = buildSchema(is);
        this.graphQL = GraphQL.newGraphQL(schema).build();
    }

    private GraphQLSchema buildSchema(InputStream is) {
        TypeDefinitionRegistry registry = new SchemaParser().parse(new InputStreamReader(is));
        RuntimeWiring wiring = buildWiring();
        SchemaGenerator generator = new SchemaGenerator();
        return generator.makeExecutableSchema(registry, wiring);
    }

    private RuntimeWiring buildWiring() {
        return RuntimeWiring.newRuntimeWiring()
                .type(TypeRuntimeWiring.newTypeWiring("Query")
                    .dataFetcher("employeeById",
                            dataFetchers.getEmployeeByIdDataFetcher()))
                .type(TypeRuntimeWiring.newTypeWiring("Employee")
                        .dataFetcher("reports",
                            dataFetchers.getReportsDataFetcher()))
                .build();
    }
}
