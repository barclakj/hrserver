package com.pxa.hr.graph;

import com.pxa.hr.model.Employee;
import com.pxa.hr.service.HRService;
import graphql.schema.DataFetcher;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
/**
 * Set of data fetchers for use in graphQL.
 */
public class HRDataFetchers {

    @Autowired
    private HRService service;

    /**
     * Returns report details to graphql.
     * @return
     */
    public DataFetcher getReportsDataFetcher() {
        return dataFetchingEnvironment -> {
            Employee employee = dataFetchingEnvironment.getSource();
            return service.getEmployeeReports(employee.getId());
        };
    }

    /**
     * Returns employee details.
     * @return
     */
    public DataFetcher getEmployeeByIdDataFetcher() {
        return dataFetchingEnvironment -> {
            try {
                int employeeId = Integer.parseInt(dataFetchingEnvironment.getArgument("id"));
                return service.getEmployee(employeeId);
            } catch (NumberFormatException e) {
                log.warn("Attempted to locate employee by invalid id. NumberFormatException: {}", e.getMessage());
                return null;
            }
        };
    }
}
