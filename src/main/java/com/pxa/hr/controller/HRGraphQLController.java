package com.pxa.hr.controller;


import com.pxa.hr.service.HRService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/graphql")
/**
 * GraphQL based controller.
 */
public class HRGraphQLController {

    @Autowired
    private HRService service;

    @PostMapping(value = "", produces = "application/json", consumes = "text/plain")
    public ResponseEntity<Map> query(@Validated @RequestBody String graphQLQuery) throws UnsupportedEncodingException {
        log.info("Received graphql query as: {}", graphQLQuery);
        String query = URLDecoder.decode(graphQLQuery,"UTF8");
        log.info("Handling graphql query as: {}", query);
        Map results = service.query(query);
        return new ResponseEntity(results, HttpStatus.OK);
    }
}
