package com.pxa.hr.controller;


import com.pxa.hr.model.Employee;
import com.pxa.hr.service.EmployeeExistsException;
import com.pxa.hr.service.HRService;
import com.pxa.hr.views.EmployeeViewAPIResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


import javax.validation.constraints.NotNull;

@Slf4j
@RestController
@RequestMapping("/employee")
/**
 * Main employee API controller.
 */
public class HRController {

    @Autowired
    private HRService service;

    @GetMapping(value = "/{employeeId}", produces = "application/json")
    public ResponseEntity<EmployeeViewAPIResponse> getEmployeeDetails(@NotNull @PathVariable("employeeId") int employeeId) {
        EmployeeViewAPIResponse employeeResult = service.getEmployeeDetails(employeeId);
        if (employeeResult==null) return new ResponseEntity(HttpStatus.NOT_FOUND);
        else return new ResponseEntity(employeeResult, HttpStatus.OK);
    }

    @DeleteMapping(value ="/{employeeId}", produces = "application/json")
    public ResponseEntity deleteEmployee(@NotNull @PathVariable("employeeId") int employeeId) {
        service.deleteEmployee(employeeId);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @PostMapping(value = "/", produces = "application/json", consumes = "application/json")
    public ResponseEntity addEmployee(@Validated @RequestBody Employee employee) {
        log.info("Handling POST request for {}", employee.getId());
        // realistically you shouldnt be able to specify the employee id on input!
        try {
            service.addNewEmployee(employee);
            HttpHeaders headers = new HttpHeaders();
            headers.add("Location", "/employee/" + employee.getId());
            return new ResponseEntity(headers, HttpStatus.CREATED);
        } catch (EmployeeExistsException e) {
            // normally would not want to send the exception details back to the client to
            // aviod leakage of internal workings but here as example considered acceptable..
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }

    }
}
