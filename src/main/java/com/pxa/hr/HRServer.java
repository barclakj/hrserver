package com.pxa.hr;


import com.pxa.hr.model.Employee;
import com.pxa.hr.service.EmployeeExistsException;
import com.pxa.hr.service.HRService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;


@Slf4j
@SpringBootApplication
/**
 * Main spring boot application.
 */
public class HRServer {
    @Autowired
    public HRService service;

    public static void main(String[] args) {
        SpringApplication.run(HRServer.class, args);
    }

    @PostConstruct
    public void setupData() {
        try {
            service.addNewEmployee(new Employee(1, "John Smith", "CEO", 0));
            service.addNewEmployee(new Employee(2, "Jane Doe", null, 1));
            service.addNewEmployee(new Employee(3, "Sally Simon", null, 1));
            service.addNewEmployee(new Employee(6, "Derek Dominoes", null, 1));
            service.addNewEmployee(new Employee(100, "Harry Henderson", null, 2));
        } catch (EmployeeExistsException e) {
            log.error("Very odd.. Can't add to an empty data set!? {}, {}", e.getMessage(), e);
        }
    }
}
