package com.pxa.hr.views;

import com.pxa.hr.model.Employee;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Wrapper class only.
 *
 * Represents an employee result object as would be returned by the API.
 * Note that here objects will contain a list of reports.
 */
public class EmployeeViewAPIResponse {
    /**
     * Wraps the employee object.
     */
    private Employee employee;
    /**
     * And the reports.
     */
    private List<Employee> reports;

    public EmployeeViewAPIResponse(Employee employee, List<Employee> reports) {
        super();
        this.employee = employee;
        this.reports = reports;
    }

    public int getId() {
        return employee.getId();
    }

    public String getName() {
        return employee.getName();
    }

    public String getTitle() {
        return employee.getTitle();
    }

    public List<Integer> getReports() {
        return reports.stream().map((employee)-> {
            return employee.getId();
        }).collect(Collectors.toList());
    }
}
