package test.pxa;

import com.pxa.hr.HRServer;
import com.pxa.hr.graph.HRGraphProvider;
import com.pxa.hr.model.Employee;
import com.pxa.hr.service.EmployeeExistsException;
import com.pxa.hr.service.HRService;
import graphql.ExecutionResult;
import graphql.GraphQLError;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = HRServer.class)
public class HRGraphQLTest {

    @Autowired
    private HRGraphProvider graphProvider;
    @Autowired
    private HRService hrService;

    @Before
    /**
     * resets data for test...
     */
    public void initData() throws EmployeeExistsException {
        hrService.deleteEmployee(1);
        hrService.deleteEmployee(2);
        hrService.deleteEmployee(100);
        hrService.deleteEmployee(3);
        hrService.deleteEmployee(6);
        hrService.addNewEmployee(new Employee(1, "John Smith", "CEO", 0));
        hrService.addNewEmployee(new Employee(2, "Jane Doe", null, 1));
        hrService.addNewEmployee(new Employee(100, "Harry Henderson", null, 2));
        hrService.addNewEmployee(new Employee(3, "Name 2", "Title 2", 1));
        hrService.addNewEmployee(new Employee(6, "Name 6", "Title 6", 1));
    }

    @Test
    public void simpleGraphQuery() {
        String query = "{ employeeById(id: \"1\") { id name title reports { name reports { name } } } }";

        ExecutionResult result = graphProvider.getGraphQL().execute(query);

        System.out.println(result.getData().getClass().getName());
        System.out.println(result.getData().toString());
    }

    @Test
    public void simpleGraphQueryWithInvalidDatatype() {
        String query = "{ \n" +
                "\temployeeById(id: \"bob\") {\n" +
                "        id\n" +
                "        name\n" +
                "        title\n" +
                "        reports {\n" +
                "        \tname\n" +
                "        \treports {\n" +
                "        \t\tname\n" +
                "        \t}\n" +
                "        }\n" +
                "    }\n" +
                "}\t";

        ExecutionResult result = graphProvider.getGraphQL().execute(query);

        System.out.println(result.toString());
    }


    @Test
    public void simpleGraphQueryWithError() {
        String query = "{ this is not a real query }";

        ExecutionResult result = graphProvider.getGraphQL().execute(query);

        List<GraphQLError> errors = result.getErrors();
        assertTrue(errors.size()>0);
        for (GraphQLError err : errors) {
            System.out.println(err.getMessage());
        }

        System.out.println(result.toString());
    }

}
