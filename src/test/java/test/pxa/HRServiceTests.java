package test.pxa;

import com.pxa.hr.HRServer;
import com.pxa.hr.model.Employee;
import com.pxa.hr.service.EmployeeExistsException;
import com.pxa.hr.service.HRService;
import com.pxa.hr.views.EmployeeViewAPIResponse;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = HRServer.class)
public class HRServiceTests {

    @Autowired
    private HRService hrService;

    @Before
    public void clearData() {
        hrService.deleteEmployee(1);
        hrService.deleteEmployee(2);
        hrService.deleteEmployee(100);
        hrService.deleteEmployee(3);
        hrService.deleteEmployee(6);
    }

    // need to be able to add the data needed to support test
    @Test
    public void canAddDataForTest() throws EmployeeExistsException {
        hrService.addNewEmployee(new Employee(1, "John Smith", "CEO", 0));
        hrService.addNewEmployee(new Employee(2, "Jane Doe", null, 1));
        hrService.addNewEmployee(new Employee(100, "Harry Henderson", null, 2));
        hrService.addNewEmployee(new Employee(3, "Name 2", "Title 2", 1));
        hrService.addNewEmployee(new Employee(6, "Name 6", "Title 6", 1));

        List<Employee> listOfEmployees = hrService.getEmployeeReports(1);
        assertTrue(listOfEmployees.size()==3);
        for(Employee e : listOfEmployees) {
            System.out.println(e.getId() + " " + e.getName() + " " + e.getTitle());
        }
    }

    @Test
    public void getEmployee() throws EmployeeExistsException {
        hrService.addNewEmployee(new Employee(1, "John Smith", "CEO", 0));
        hrService.addNewEmployee(new Employee(2, "Jane Doe", null, 1));
        EmployeeViewAPIResponse emp = hrService.getEmployeeDetails(1);

        assertNotNull(emp);
        assertTrue(emp.getReports().size()==1); // should only have jane doe
        String reports = emp.getReports().stream().map(report -> report.toString()).collect(Collectors.joining(", "));
        System.out.println(emp.getId() + " " + emp.getName() + " " + emp.getTitle() + " " + reports);
    }

    @Test
    public void canAddAndFetchEmployee() throws EmployeeExistsException {
        hrService.addNewEmployee(new Employee(1, "John Smith", "CEO", 0));
        Employee emp = hrService.getEmployee(1);
        Assert.assertNotNull(emp);
        Assert.assertEquals(emp.getName(), "John Smith");
        Assert.assertEquals(emp.getTitle(), "CEO");
    }

    @Test
    public void canAddEmployeeAndReports() throws EmployeeExistsException {
        hrService.addNewEmployee(new Employee(1, "John Smith", "CEO", 0));
        hrService.addNewEmployee(new Employee(2, "Employee 2", "T2", 1));
        hrService.addNewEmployee(new Employee(3, "Employee 3", "T3", 1));
        hrService.addNewEmployee(new Employee(6, "Employee 6", "T6", 1));
        hrService.addNewEmployee(new Employee(100, "Henderson", "T100", 2));

        List<Integer> reportees = hrService.getEmployeeReports(1).stream()
                .map(employee -> {
                    return employee.getId();
                })
                .collect(Collectors.toList());
        Employee emp = hrService.getEmployee(1);
        Assert.assertTrue(reportees.contains(2));
        Assert.assertTrue(reportees.contains(3));
        Assert.assertTrue(reportees.contains(6));
    }

    @Test
    public void canDeleteEmployee() throws EmployeeExistsException {
        int id = (int)(Math.random()*100.);
        hrService.addNewEmployee(new Employee(id, "Some name", "some role", 0));
        Employee emp = hrService.getEmployee(id);
        Assert.assertNotNull(emp);
        hrService.deleteEmployee(id);
        Assert.assertNull(hrService.getEmployee(id));
    }

    @Test
    public void canListEmployeeReports() throws EmployeeExistsException {
        hrService.addNewEmployee(new Employee(1, "John Smith", "CEO", 0));
        hrService.addNewEmployee(new Employee(2, "Employee 2", "T2", 1));
        hrService.addNewEmployee(new Employee(3, "Employee 3", "T3",1));
        hrService.addNewEmployee(new Employee(6, "Employee 6", "T6", 1));

        List<Employee> employeeList = hrService.getEmployeeReports(1);
        Assert.assertNotNull(employeeList);
        Assert.assertTrue(employeeList.size()==3);
    }


    // need to be able to add the data needed to support test
    @Test(expected = EmployeeExistsException.class)
    public void getExceptioOnExistingEmpAdd() throws EmployeeExistsException {
        hrService.addNewEmployee(new Employee(1, "John Smith", "CEO", 0));
        hrService.addNewEmployee(new Employee(1, "SHOULD FAIL", "SHOULD FAIL", 0));

    }
}
