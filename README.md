# HR Server

Build should be accomplished via a simple.. 

mvn clean package

Application is configured to run on port 10080 and should run
via

java -jar hrserver-0.1-SNAPSHOT.jar

or

mvn clean package spring-boot:run

to verify output open http://localhost:10080/employee/1
in a browser.

Desired output of the client appears to be 
ideally suited to graphQL but the prescribed
API spec doesn't allow for this. A dummy graphQL
implementation is included for the heck of it and can 
be accessed by posting graphQL to http://localhost:10080/graphql

This is best demo'd via curl such as:

curl -X POST -H "Content-type: text/plain" http://localhost:10080/graphql --data "@query.gql"

where the file query.gql contains a graphql query such as:


{ employeeById(id: "1") { id name title reports { id name reports { name } } } }


New employees can be added using the command below:

curl -X POST -H "Content-type: application/json" http://localhost:10080/employee/ --data "@newemployee.json"

which each employee as json such as:

{ "id": 10, "name": "Bob", "title": "Tea Lady", "reportsTo": 1}

Deleting an employee equally achieved via:

curl -X DELETE http://localhost:10080/employee/{employeeId}
